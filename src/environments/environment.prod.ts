export const environment = {
  production: true,
  apiUrlBase: 'http://localhost:8080/v1',
  loginUrl: 'http://localhost:8080/login'
};
