import { Component, OnInit } from '@angular/core';
import { distinctUntilChanged, debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../_services/user.service';
import { UserGetDto } from '../_dtos/userGetDto';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public user: UserGetDto;
  username:string;
  users:Array<UserGetDto>;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.users.filter(v => v.username.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )
   
    formatter = (x: {username: string}) => x.username;
    
  constructor(private authService: AuthService, 
    private router: Router, 
    private userService: UserService) { }

  ngOnInit() {
    this.username="";
    this.user= new UserGetDto();
    this.userService.getUser(localStorage.getItem("id_user")).subscribe((user:UserGetDto)=>{
      this.username=user.username;
    });
  }

  getAllUsers(){
    this.userService.getAllUsers().subscribe((users:Array<UserGetDto>)=>{
      this.users=users;
    });
  }

  find(){
    this.router.navigate(['timeline'],{ queryParams: { user: this.user.id } })
  }

  exit(){
    this.authService.logout();
    this.router.navigate(['']);
  }

}
