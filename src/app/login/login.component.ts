import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading:boolean;
  mEmail: string;
  mPassword: string;
  error:boolean;
  emailNotFound:boolean;
  errorMessage:string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.mEmail="";
    this.mPassword="";
    this.emailNotFound=false;
    this.errorMessage="";
  }

  onSubmit() { 
    this.loading = true;
    this.authService.login(this.mEmail,this.mPassword).subscribe(() => {
      this.loading = false;
      this.error=false;
      this.router.navigate(['/dashboard'])
     },
     error=> {
      this.loading = false;
      if(error===404){
        this.error=false;
        this.emailNotFound=true;
        this.errorMessage="Email no registrado";
       }
       else {
        this.error=true;
        this.errorMessage="Email o contraseña incorrecta";
       }

     });
  }

  valuechange(newValue) {
    if(this.emailNotFound===true){
      this.emailNotFound=false;
    }
  }

 
}
