import { HomeComponent } from "./home/home.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { RegistryComponent } from "./registry/registry.component";
import { AccountCreatedComponent } from "./account-created/account-created.component";
import { TimelineComponent } from "./timeline/timeline.component";
import { AuthGuard } from "./_guards/auth.guard";

const appRoutes: Routes = [
    {path:'', component: HomeComponent},
    {path:'dashboard', component: DashboardComponent,canActivate: [AuthGuard]},
    {path:'login', component: LoginComponent},
    {path:'registro', component: RegistryComponent},
    {path:'cuenta_creada', component: AccountCreatedComponent}, 
    {path:'timeline', component: TimelineComponent, canActivate: [AuthGuard]},     
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);