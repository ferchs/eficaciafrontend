import { Component, OnInit } from '@angular/core';
import { TimelineService } from '../_services/timelime.service';
import { UserMessageDto } from '../_dtos/userMessageDto';
import { MessageDto } from '../_dtos/messageDto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loading:boolean;
  efiTweet:string;
  messages:Array<MessageDto>;


  constructor(private timelineService:TimelineService) { }

  ngOnInit() {
    this.efiTweet="";
    this.messages= new Array<MessageDto>();
    this.getAllMessages();
  }

  publishTwett(){
    this.loading = true;
    let userMessage:UserMessageDto= new UserMessageDto();
    userMessage.message=this.efiTweet;
    this.timelineService.createMessage(userMessage).subscribe(()=>{
      this.loading = false;
      this.efiTweet="";
      this.getAllMessages();
    });
  }

  getAllMessages(){
    this.timelineService.getMessagesOfUser(localStorage.getItem("id_user")).subscribe((messages:Array<MessageDto>)=>{
      this.messages=messages;
    });
  }

  noFoundMessages(){
    if(this.messages==null || this.messages==undefined){
      return true;
    }else{
      return false;
    }
  }

}
