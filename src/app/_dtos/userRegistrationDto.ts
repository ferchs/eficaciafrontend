export class UserRegistrationDto {

    public username:string;

    public name:string;
  
    public lastname:string;

    public email:string;
      
    public password:string;
      
    public matchingPassword:string;
                                  
}