export class UserGetDto {

    public id:string;

    public username:string;

    public name:string;
  
    public lastname:string;

    public email:string;

    constructor(){
        this.id="";
        this.username="";
        this.name="";
        this.lastname="";
        this.email="";
    }
      
}