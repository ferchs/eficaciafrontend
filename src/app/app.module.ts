import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { routing } from './app.routing';
import { RegistryComponent } from './registry/registry.component';
import { LoginComponent } from './login/login.component';
import { LoadingModule } from 'ngx-loading';
import { AuthGuard } from './_guards/auth.guard';
import { AuthService } from './_services/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TimelineComponent } from './timeline/timeline.component';
import { UserService } from './_services/user.service';
import { AccountCreatedComponent } from './account-created/account-created.component';
import { TimelineService } from './_services/timelime.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    DashboardComponent,
    RegistryComponent,
    LoginComponent,
    TimelineComponent,
    AccountCreatedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    LoadingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    routing
  ],
  providers: [AuthService,AuthGuard,UserService,TimelineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
