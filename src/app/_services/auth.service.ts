import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
import * as moment from "moment";

interface Token {
    expiresIn: number;
    userId: number;
    providerId: number;
}

@Injectable()
export class AuthService {
    
    constructor(private http: HttpClient) { }
    
    login(email:string, password:string){
        return this.http.post(environment.loginUrl,{email, password},{observe: 'response'})
        .pipe(
            map((res:HttpResponse<Token>) => {  
                this.setSession(res);
                return res.status;
            }),
            catchError(this.handleError)
          );
    }

    private handleError (error: Response) {
        console.log("Se esta manejando un error");
          return Observable.throw(error.status);        
      }

    private setSession(authResult:HttpResponse<Token>) {
        const expiresAt = moment(authResult.body.expiresIn);
        localStorage.setItem('id_token', authResult.headers.get('Authorization'));
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
        localStorage.setItem("id_user", JSON.stringify(authResult.body.userId.valueOf()));
    }    

    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
        localStorage.removeItem("id_user");
    }

    
    public isLoggedIn() {
        if(this.getExpiration()!=null){
            if(moment().isBefore(this.getExpiration())){
                return true;
            }else{
                this.logout();
                return false;
            }
        }else{
            return false;
        }
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        if(localStorage.getItem("expires_at")!= null){
            const expiration = localStorage.getItem("expires_at");
            const expiresAt = JSON.parse(expiration);
            return moment(expiresAt);
        } else{
            return null;
        }
    }  
}