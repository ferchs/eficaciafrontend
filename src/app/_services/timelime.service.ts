import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/throw';
import { UserGetDto } from '../_dtos/userGetDto';
import { UserRegistrationDto } from '../_dtos/userRegistrationDto';
import { UserMessageDto } from '../_dtos/userMessageDto';
import { MessageDto } from '../_dtos/messageDto';

@Injectable()
export class TimelineService {

  private apiUrl = environment.apiUrlBase+"/users";

  constructor(private http: HttpClient) {
  }

  getMessagesOfUser(id:string){
    return this.http.get<Array<MessageDto>>(this.apiUrl+"/"+id+"/messages").pipe(
      map((userMessages:Array<MessageDto>)=> { 
        return userMessages;
      }),
      catchError(this.handleError)
    );
  }

  createMessage(content:UserMessageDto):Observable<number>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});          
    return this.http.post(this.apiUrl+"/"+ localStorage.getItem("id_user")+"/messages", content, {headers: headers, observe: 'response', responseType: 'text'})
    .pipe(
      map(response => { return response.status;}),
      catchError(this.handleError)
    );
    }

    private handleError (error: Response) {
      console.log("Se esta manejando un error");
        return Observable.throw(error.status);        
    }

}