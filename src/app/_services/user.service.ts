import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/throw';
import { Subject } from 'rxjs/Subject';
import { UserGetDto } from '../_dtos/userGetDto';
import { UserRegistrationDto } from '../_dtos/userRegistrationDto';

@Injectable()
export class UserService {

  private apiUrl = environment.apiUrlBase+"/users";

  constructor(private http: HttpClient) {
  }

  getUser(id:string){
    return this.http.get<UserGetDto>(this.apiUrl+"/"+id)
    .pipe(
      map((userInfo:UserGetDto)=> { 
        return userInfo
      }),
      catchError(this.handleError)
    );
  }

  getAllUsers(){
    return this.http.get<Array<UserGetDto>>(this.apiUrl).pipe(
      map((users:Array<UserGetDto>)=> { 
        return users;
      }),
      catchError(this.handleError)
    );
  }

  createUser(user: UserRegistrationDto):Observable<number>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});          
    return this.http.post(this.apiUrl, user, {headers: headers, observe: 'response', responseType: 'text'})
    .pipe(
      map(response => { return response.status;}),
      catchError(this.handleError)
    );
    }

    private handleError (error: Response) {
      console.log("Se esta manejando un error");
        return Observable.throw(error.status);        
    }

}