import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TimelineService } from '../_services/timelime.service';
import { MessageDto } from '../_dtos/messageDto';
import { UserGetDto } from '../_dtos/userGetDto';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  loading:boolean;
  user:UserGetDto;
  messages= Array<MessageDto>();


  constructor(private activatedRoute: ActivatedRoute,
    private timelineService:TimelineService,
    private userService:UserService) { }

  ngOnInit() {
    this.loading = true;
    this.messages= new Array<MessageDto>();
    this.user= new UserGetDto();
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.userService.getUser(params['user']).subscribe((user:UserGetDto)=> {
        this.user=user;
        this.getAllMessages(user.id);
        this.loading = false;
      });
    });
  }

  getAllMessages(id:string){
    this.timelineService.getMessagesOfUser(id).subscribe((messages:Array<MessageDto>)=>{
      this.messages=messages;
    });
  }

  noFoundMessages(){
    if(this.messages==null || this.messages==undefined){
      return true;
    }else{
      return false;
    }
  }

}
