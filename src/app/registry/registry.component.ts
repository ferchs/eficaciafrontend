import { Component, OnInit } from '@angular/core';
import { UserRegistrationDto } from '../_dtos/userRegistrationDto';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {

  dto:UserRegistrationDto;
  registeredEmailError:boolean;
  loading:boolean;
  public loggedIn:boolean;

  
  ngOnInit() {
    this.dto= new UserRegistrationDto();    
    this.registeredEmailError=false;
    this.loading=false; 
  }
  
  constructor(private userService: UserService,
    private router: Router) {}

  onSubmit(){
    this.loading = true;

    this.userService.createUser(this.dto)
    .subscribe(httpCode => {
      this.loading = false;
      this.router.navigate(['cuenta_creada'])
     },
     error=> {
      this.loading = false;
       if(error===409){
        this.registeredEmailError=true;
       }
     });
    }

    valuechange(newValue) {
      if(this.registeredEmailError===true){
        this.registeredEmailError=false;
      }
    }

}
